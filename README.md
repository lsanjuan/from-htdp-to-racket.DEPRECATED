----------------------------------------------------------------
**WARNING!!**

**This repo has been discontinued, and it is now DEPRECATED.**
----------------------------------------------------------------


# From HtDP to Racket

This repository contains source code that shows by means of an example how 
to transform a program written in HtDP teaching languages 
(specifically, ISL+) into Racket code.

## Source code

- `book-info`:
  A module for retrieving and parsing bibliographic information.

- `isbn-isl-with-batteries.rkt`: 
  An ISBN extractor written in ISL+ with regexes.

- `isbn-racket.v1.rkt`:
  Racket transformation of `isbn-isl-with-batteries.rkt`: 
  `#lang racket/base`, local definitions with `define`, and other 
  minor tweaks.

- `isbn-racket.v2.rkt`:
  Test submodules, `rackunit`, `only-in` in `require` added to 
  `isbn-racket.v1.rkt`.

- `isbn-racket.v3.rkt`: 
  `for`, `match`, and sequences added to `isbn-racket.v2.rkt`.

- `isbn-racket.v4.rkt`: 
  `provide`, and contracts added to `isbn-racket.v3.rkt`

- `isbn.rkt`:
  Input streams, optional and keyword arguments in functions, point-free 
  style function definitions, and minor tweaks added to 
  `isbn-racket.v4.rkt`.

- `main-example.rkt`:
  A very simple client that uses the Racket code in this directory. 

- `pdf-isbn.rkt`:
  A module for extracting the ISBN from a pdf document.

## Examples for test cases

- `test-isbn-examples`

- `test-isbn-examples.pdf`

- `test-with-isbn-10-only.pdf`

- `test-without-isbn.pdf`
  
